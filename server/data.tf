data "aws_caller_identity" "current" {}

data "aws_s3_bucket" "primary" {
  bucket = var.bucket_id
}

data "aws_vpc" "primary" {
  tags = {
    Name = var.environment
  }
}

data "aws_route53_zone" "primary" {
  name = data.aws_vpc.primary.tags["DNSZone"]
}

data "aws_subnet_ids" "primary" {
  vpc_id = data.aws_vpc.primary.id

  tags = {
    "Type" = var.subnet_type
  }
}

data "aws_subnet" "primary" {
  for_each = data.aws_subnet_ids.primary.ids
  id       = each.value
}

data "aws_ami" "primary" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}
