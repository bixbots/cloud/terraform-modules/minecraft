variable "region" {
  type        = string
  default     = "us-east-1"
  description = "(Optional; Default: us-east-1) AWS region where to create resources."
}

variable "lifespan" {
  type        = string
  default     = "permanent"
  description = "(Optional; Default: permanent) The intended lifespan of the resource(s)."
}

variable "application" {
  type        = string
  default     = "minecraft"
  description = "(Optional; Default: minecraft) Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of."
}

variable "role" {
  type        = string
  default     = "server"
  description = "(Optional; Default: server) Name of the role the resource(s) will be performing."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}

#

variable "bucket_id" {
  type        = string
  description = "The ID of the bucket for storing minecraft files."
}

variable "timezone" {
  type        = string
  description = "The timezone of the server using TZ database name formatting (i.e. `America/New_York`)."
}

variable "cert_subject" {
  type        = string
  description = "The subject of the SSL certificate to use for nginx."
}

#

variable "users" {
  type        = list(string)
  description = "Specifies the list of allowed users (i.e. whitelist) for the minecraft server. Must be valid Mojang profile names."
}

variable "operators" {
  type        = list(string)
  description = "Specifies the list of operators (i.e. op) for the minecraft server. Must be valid Mojang profile names. These operators will also be added to the whitelist users."
}

variable "gamemode" {
  type        = string
  default     = "survival"
  description = "(Optional; Default: survival) Defines the mode of gameplay. See https://minecraft.gamepedia.com/Server.properties for more information."
}

variable "difficulty" {
  type        = string
  default     = "normal"
  description = "(Optional; Default: normal) Defines the difficulty (such as damage dealt by mobs and the way hunger and poison affects players) of the server. See https://minecraft.gamepedia.com/Server.properties for more information."
}

variable "motd" {
  type        = string
  default     = ""
  description = "(Optional; Default: empty) This is the message that is displayed in the server list of the client, below the name. See https://minecraft.gamepedia.com/Server.properties for more information."
}

variable "java_mem" {
  type        = string
  default     = "4096M"
  description = "(Optional; Default: 4096M) The amount of memory to use for the Java process."
}

variable "max_players" {
  type        = number
  default     = 20
  description = "(Optional; Default: 20) The maximum number of players that can play on the server at the same time. See https://minecraft.gamepedia.com/Server.properties for more information."
}

variable "rcon_pwd" {
  type        = string
  description = "Sets the password for RCON: a remote console protocol that can allow other applications to connect and interact with a Minecraft server over the internet. See https://minecraft.gamepedia.com/Server.properties for more information."
}

# 

variable "subnet_type" {
  type        = string
  default     = "public"
  description = "(Optional; Default: public) Specifies which subnet, either `public`, `private`, or `data`, the instance(s) are launched in."
}

variable "instance_type" {
  type        = string
  description = "The type of EC2 instance to use. If the instance type changes, a new launch configuration will be created and added to the ASG."
}

variable "spot_price" {
  type        = string
  default     = ""
  description = "(Optional; Default: On-demand price) The maximum price to use for reserving spot instances. If omitted then spot instance will not be used."
}

variable "key_name" {
  type        = string
  description = "Name of the SSH keypair used to access launched instances."
}

variable "root_block_device" {
  type = object({
    enabled     = bool
    volume_type = string
    volume_size = number
  })
  default = {
    enabled     = true
    volume_type = "gp2"
    volume_size = 20
  }
  description = "(Optional, Default: enabled with 20GB) The size of the root volume in gigabytes."
}
