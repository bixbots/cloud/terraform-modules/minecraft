# minecraft/server

## Summary

This module creates a minecraft server with the following features:

* Running [PaperMC](https://paper.readthedocs.io/en/latest/)
* Plugins automatically loaded from S3 bucket
* Configurable settings for `server.properties` such as whitelist, gamemode, etc
* Using [duplicity](http://duplicity.nongnu.org/) for backups (full and incremental) to S3 bucket
* Backup is automatically restored when ASG redeploys the server
* Dynmap plugin with web map hosted in nginx with SSL/HTTPS certificate
* Command line client for RCON

## Resources

In addition to these dependent modules:

* [launch_pad](https://gitlab.com/bixbots/cloud/terraform-modules/launch-pad)
* [cloud-init-dns](https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-dns)
* [cloud-init-acme-nginx](https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-acme-nginx)
* [standalone-ec2](https://gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2)

This module creates the following resources:

* [aws_iam_policy](https://www.terraform.io/docs/providers/aws/r/iam_policy.html)
* [aws_iam_role_policy_attachment](https://www.terraform.io/docs/providers/aws/r/iam_role_policy_attachment.html)
* [aws_security_group_rule](https://www.terraform.io/docs/providers/aws/r/security_group_rule.html)

## Cost

Use of this module will result in billable resources being launched in AWS. Before using this module, please familiarize yourself with the expenses associated with EC2 instances and autoscaling groups.

* AWS EC2 pricing information is available [here](https://aws.amazon.com/ec2/pricing/)

## Inputs

| Name             | Description                                                                                                                                                                                                                            | Type           | Default     | Required |
|:-----------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:------------|:---------|
| region           | AWS region where to create resources.                                                                                                                                                                                                  | `string`       | `us-east-1` | yes      |
| lifespan         | The intended lifespan of the resource(s).                                                                                                                                                                                              | `string`       | `permanent` | yes      |
| application      | Name of the application the resource(s) will be a part of.                                                                                                                                                                             | `string`       | `minecraft` | yes      |
| environment      | Name of the environment the resource(s) will be a part of.                                                                                                                                                                             | `string`       | -           | yes      |
| role             | Name of the role the resource(s) will be performing.                                                                                                                                                                                   | `string`       | `server`    | yes      |
| bucket_id        | The ID of the bucket for storing minecraft files.                                                                                                                                                                                      | `string`       | -           | yes      |
| timezone         | The timezone of the server using TZ database name formatting (i.e. `America/New_York`).                                                                                                                                                | `string`       | -           | yes      |
| cert_subject     | The subject of the SSL certificate to use for nginx.                                                                                                                                                                                   | `string`       | -           | yes      |
| users            | Specifies the list of allowed users (i.e. whitelist) for the minecraft server. Must be valid Mojang profile names.                                                                                                                     | `list(string)` | -           | yes      |
| operators        | Specifies the list of operators (i.e. op) for the minecraft server. Must be valid Mojang profile names. These operators will also be added to the whitelist users.                                                                     | `list(string)` | -           | yes      |
| gamemode         | Defines the mode of gameplay. See https://minecraft.gamepedia.com/Server.properties for more information.                                                                                                                              | `string`       | `survival`  | yes      |
| difficulty       | Defines the difficulty (such as damage dealt by mobs and the way hunger and poison affects players) of the server. See https://minecraft.gamepedia.com/Server.properties for more information.                                         | `string`       | `normal`    | yes      |
| motd             | This is the message that is displayed in the server list of the client, below the name. See https://minecraft.gamepedia.com/Server.properties for more information.                                                                    | `string`       | -           | no       |
| java_mem         | The amount of memory to use for the Java process.                                                                                                                                                                                      | `string`       | `4096M`     | yes      |
| max_players      | The maximum number of players that can play on the server at the same time. See https://minecraft.gamepedia.com/Server.properties for more information.                                                                                | `number`       | 20          | yes      |
| rcon_pwd         | Sets the password for RCON: a remote console protocol that can allow other applications to connect and interact with a Minecraft server over the internet. See https://minecraft.gamepedia.com/Server.properties for more information. | `string`       | -           | yes      |
| subnet_type      | Specifies which subnet, either `public`, `private`, or `data`, the instance(s) are launched in.                                                                                                                                        | `string`       | `public`    | yes      |
| instance_type    | The type of EC2 instance to use. If the instance type changes, a new launch configuration will be created and added to the ASG.                                                                                                        | `string`       | -           | yes      |
| spot_price       | The maximum price to use for reserving spot instances. If omitted then spot instance will not be used.                                                                                                                                 | `string`       | -           | no       |
| key_name         | Name of the SSH keypair used to access launched instances.                                                                                                                                                                             | `string`       | -           | yes      |
| root_volume_size | The size of the root volume in gigabytes.                                                                                                                                                                                              | `number`       | 10          | yes      |
| tags             | Additional tags to attach to all resources created by this module.                                                                                                                                                                     | `map(string)`  | `{}`        | no       |

## Outputs

This module doesn't output anything.

## Requirements

| Name      | Version    |
|:----------|:-----------|
| terraform | ~> 0.12.24 |
| aws       | ~> 2.42    |

## Examples

### Simple Usage

```terraform
module "bucket" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/minecraft//bucket?ref=v2"

  region      = "us-east-1"
  lifespan    = "permanent"
  application = "minecraft"
  environment = "example"
  role        = "server"
  tags        = {}
}
```

## Version History

* v1 - Initial Release
* v2 - Use papermc vs spigot and upgrade to 1.16
