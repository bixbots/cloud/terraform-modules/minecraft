#!/usr/bin/env bash

VAR_REGION='${var_region}'
VAR_FQDN='${var_fqdn}'
VAR_BUCKET_ID='${var_bucket_id}'

VAR_TIMEZONE='${var_timezone}'
VAR_OPERATORS_JSON='${var_operators_json}'
VAR_USERS_JSON='${var_users_json}'
VAR_ENABLE_WHITELIST='${var_enable_whitelist}'
VAR_GAMEMODE='${var_gamemode}'
VAR_DIFFICULTY='${var_difficulty}'
VAR_MAX_PLAYERS='${var_max_players}'
VAR_MOTD='${var_motd}'
VAR_JAVA_MEM='${var_java_mem}'
VAR_RCON_PWD='${var_rcon_pwd}'

VAR_SAVE_WAIT="10s"

VAR_BACKUP_DIR="/var/minecraft/server"
VAR_S3_URL_BASE="s3://s3.amazonaws.com/$VAR_BUCKET_ID"
VAR_S3_URL="$VAR_S3_URL_BASE/backup"

export AWS_DEFAULT_REGION="$VAR_REGION"

sudo hostnamectl set-hostname $VAR_FQDN

sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm > /dev/null
sudo yum-config-manager --enable epel > /dev/null
sudo yum update -y > /dev/null

sudo yum install -y amazon-linux-extras > /dev/null
sudo yum install -y nginx > /dev/null
sudo amazon-linux-extras install -y php7.4 > /dev/null

sudo yum install -y jq git gcc java-1.8.0-openjdk-headless > /dev/null
sudo yum install -y python2 python2-pip duplicity rsync gpg > /dev/null

sudo useradd --system --user-group --create-home -K UMASK=0022 --home /var/minecraft minecraft
sudo mkdir -p /var/minecraft/{build/mcrcon,server/plugins}

cat > /var/minecraft/minecraft-rcon.sh <<EOF
#!/usr/bin/env bash

RCON_PWD="$VAR_RCON_PWD"

if [ "\$1" != "" ] ; then
  (set -x; /var/minecraft/mcrcon -c -H localhost -P 25575 -p \$RCON_PWD "\$1") 2>&1 || true
else
  (set -x; /var/minecraft/mcrcon -c -H localhost -P 25575 -p \$RCON_PWD -t) 2>&1 || true
fi
EOF
sudo chmod +x /var/minecraft/minecraft-rcon.sh

cat > /var/minecraft/minecraft-start.sh <<EOF
#!/usr/bin/env bash

pushd /var/minecraft/server

(set -x; wget -q https://papermc.io/api/v1/paper/1.16.1/latest/download -O paperclip.jar) 2>&1

(set -x; /usr/bin/java \
  -Xms$VAR_JAVA_MEM \
  -Xmx$VAR_JAVA_MEM \
  -XX:+UseG1GC \
  -XX:+ParallelRefProcEnabled \
  -XX:MaxGCPauseMillis=200 \
  -XX:+UnlockExperimentalVMOptions \
  -XX:+DisableExplicitGC \
  -XX:+AlwaysPreTouch \
  -XX:G1NewSizePercent=30 \
  -XX:G1MaxNewSizePercent=40 \
  -XX:G1HeapRegionSize=8M \
  -XX:G1ReservePercent=20 \
  -XX:G1HeapWastePercent=5 \
  -XX:G1MixedGCCountTarget=4 \
  -XX:InitiatingHeapOccupancyPercent=15 \
  -XX:G1MixedGCLiveThresholdPercent=90 \
  -XX:G1RSetUpdatingPauseTimePercent=5 \
  -XX:SurvivorRatio=32 \
  -XX:+PerfDisableSharedMem \
  -XX:MaxTenuringThreshold=1 \
  -Dusing.aikars.flags=https://mcflags.emc.gs \
  -Daikars.new.flags=true \
  -jar paperclip.jar \
  nogui \
  --noconsole \
  ) 2>&1

popd
EOF
sudo chmod +x /var/minecraft/minecraft-start.sh

cat > /var/minecraft/minecraft-stop.sh <<EOF
#!/usr/bin/env bash

RCON_PWD="$VAR_RCON_PWD"

(set -x; /var/minecraft/mcrcon -H localhost -P 25575 -p \$RCON_PWD "stop" >/dev/null 2>&1) 2>&1 || true
EOF
sudo chmod +x /var/minecraft/minecraft-stop.sh

cat > /var/minecraft/backup-incremental.sh <<EOF
#!/usr/bin/env bash

USERNAME="minecraft"
SERVICE="paperclip.jar"

if pgrep -u \$USERNAME -f \$SERVICE > /dev/null ; then
  RCON_PWD="$VAR_RCON_PWD"
  (set -x; /var/minecraft/mcrcon -H localhost -P 25575 -p \$RCON_PWD "save-all" >/dev/null 2>&1) 2>&1 || true
  sleep $VAR_SAVE_WAIT
fi

BACKUP_FOLDER="\$1"
if [ "\$BACKUP_FOLDER" == "" ] ; then
  BACKUP_FOLDER="backup"
fi

S3_URL_BASE="$VAR_S3_URL_BASE"
S3_URL="\$S3_URL_BASE/\$BACKUP_FOLDER"
BACKUP_DIR="$VAR_BACKUP_DIR"

echo "Backup Incremental Begin"
(set -x; duplicity incremental \$BACKUP_DIR \$S3_URL --no-encryption) 2>&1
echo "Backup Incremental Complete"
EOF
sudo chmod +x /var/minecraft/backup-incremental.sh

cat > /var/minecraft/backup-full.sh <<EOF
#!/usr/bin/env bash

USERNAME="minecraft"
SERVICE="paperclip.jar"

if pgrep -u \$USERNAME -f \$SERVICE > /dev/null ; then
  RCON_PWD="$VAR_RCON_PWD"
  (set -x; /var/minecraft/mcrcon -H localhost -P 25575 -p \$RCON_PWD "save-all" >/dev/null 2>&1) 2>&1 || true
  sleep $VAR_SAVE_WAIT
fi

BACKUP_FOLDER="\$1"
if [ "\$BACKUP_FOLDER" == "" ] ; then
  BACKUP_FOLDER="backup"
fi

S3_URL_BASE="$VAR_S3_URL_BASE"
S3_URL="\$S3_URL_BASE/\$BACKUP_FOLDER"
BACKUP_DIR="$VAR_BACKUP_DIR"

echo "Backup Full Begin"
(set -x; duplicity full \$BACKUP_DIR \$S3_URL --no-encryption) 2>&1
echo "Backup Full Complete"
EOF
sudo chmod +x /var/minecraft/backup-full.sh

cat > /var/minecraft/backup-restore.sh <<EOF
#!/usr/bin/env bash

USERNAME="minecraft"
SERVICE="paperclip.jar"
STATUS=""

if pgrep -u \$USERNAME -f \$SERVICE > /dev/null ; then
  STATUS="running"
  (set -x; systemctl stop minecraft && sleep $VAR_SAVE_WAIT) 2>&1
fi

BACKUP_FOLDER="\$1"
if [ "\$BACKUP_FOLDER" == "" ] ; then
  BACKUP_FOLDER="backup"
fi

S3_URL_BASE="$VAR_S3_URL_BASE"
S3_URL="\$S3_URL_BASE/\$BACKUP_FOLDER"
BACKUP_DIR="$VAR_BACKUP_DIR"

echo "Backup Restore Begin"
(set -x; duplicity --force restore \$S3_URL \$BACKUP_DIR --no-encryption || echo "Backup restore failed") 2>&1
echo "Backup Restore Complete"

if [ "\$STATUS" == "running" ] ; then
  (set -x; systemctl start minecraft) 2>&1
fi
EOF
sudo chmod +x /var/minecraft/backup-restore.sh

cat > /var/minecraft/backup-prune.sh <<EOF
#!/usr/bin/env bash

BACKUP_FOLDER="\$1"
if [ "\$BACKUP_FOLDER" == "" ] ; then
  BACKUP_FOLDER="backup"
fi

S3_URL_BASE="$VAR_S3_URL_BASE"
S3_URL="\$S3_URL_BASE/\$BACKUP_FOLDER"

echo "Backup Prune Begin"
(set -x; duplicity remove-all-but-n-full 1 \$S3_URL --force --no-encryption) 2>&1
echo "Backup Prune Complete"
EOF
sudo chmod +x /var/minecraft/backup-prune.sh

sudo chown minecraft.minecraft -R /var/minecraft/

# prepare mcrcon
sudo su - minecraft -c 'cd /var/minecraft/build/mcrcon && git clone https://github.com/Tiiffi/mcrcon.git && cd mcrcon && make && cp mcrcon ~/'

# restore files from backup
sudo su - minecraft -c '/var/minecraft/backup-restore.sh'

# download files from s3
sudo aws s3 cp s3://$VAR_BUCKET_ID/files/minecraft /var/minecraft/ --recursive
sudo aws s3 cp s3://$VAR_BUCKET_ID/files/nginx /usr/share/nginx/ --recursive
sudo chown minecraft.minecraft -R /var/minecraft/

# generate JSON for whitelist
echo "[$(echo $VAR_USERS_JSON | \
  jq '.[]' | \
  xargs -L1 -I'{}' curl -s 'https://playerdb.co/api/player/minecraft/{}' | \
  jq --compact-output '{ "uuid": .data.player.id, "name": .data.player.username }' | \
  paste -sd "," -)]" | \
  jq '.' \
  > /var/minecraft/server/whitelist.json

# generate JSON for operators
echo "[$(echo $VAR_OPERATORS_JSON | \
  jq '.[]' | \
  xargs -L1 -I'{}' curl -s 'https://playerdb.co/api/player/minecraft/{}' | \
  jq --compact-output '{ "uuid": .data.player.id, "name": .data.player.username, "level": 4, "bypassesPlayerLimit": false }' | \
  paste -sd "," -)]" | \
  jq '.' \
  > /var/minecraft/server/ops.json

cat > /var/minecraft/server/server.properties <<EOF
gamemode=$VAR_GAMEMODE
difficulty=$VAR_DIFFICULTY
max-players=$VAR_MAX_PLAYERS
allow-flight=true
white-list=$VAR_ENABLE_WHITELIST
enable-query=true
enable-rcon=true
rcon.password=$VAR_RCON_PWD
server-port=25565
query.port=25565
rcon.port=25575
motd=$VAR_MOTD
EOF

sudo chown minecraft.minecraft -R /var/minecraft/

cat > /etc/nginx/default.d/minecraft.conf <<EOF
location /map {
  alias /var/minecraft/server/plugins/dynmap/web/;
}
EOF

cat > /etc/systemd/system/minecraft.service <<EOF
[Unit]
Description=Minecraft Server

Wants=network.target
After=network.target

[Service]
User=minecraft
Group=minecraft
Nice=5
EnvironmentFile=-/var/minecraft/unit.conf
KillMode=none
SuccessExitStatus=0
Restart=on-failure
RestartSec=5s

ProtectHome=true
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
PrivateTmp=true
InaccessibleDirectories=/root /sys /srv -/opt /media -/lost+found
ReadWriteDirectories=/var/minecraft/server
WorkingDirectory=/var/minecraft/server
ExecStart=/var/minecraft/minecraft-start.sh
ExecStop=/var/minecraft/minecraft-stop.sh

[Install]
WantedBy=multi-user.target
EOF

cat > /etc/cron.d/minecraft <<EOF
*/10 * * * * minecraft /var/minecraft/backup-incremental.sh
*/30 * * * * minecraft /var/minecraft/backup-full.sh
5 * * * * minecraft /var/minecraft/backup-prune.sh
EOF

cat > /etc/sysconfig/clock <<EOF
ZONE="$VAR_TIMEZONE"
UTC=true
EOF
sudo ln -sf /usr/share/zoneinfo/$VAR_TIMEZONE /etc/localtime

sudo systemctl enable nginx
sudo systemctl enable minecraft
sudo shutdown -r +1
