resource "aws_security_group_rule" "allow_ssh_from_all" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_http_from_all" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_https_from_all" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_minecraft_from_all" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 25565
  to_port   = 25565
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}
