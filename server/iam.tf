data "aws_iam_policy_document" "bucket_admin" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions",
      "s3:ListBucketMultipartUploads",
    ]
    resources = [data.aws_s3_bucket.primary.arn]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:AbortMultipartUpload",
      "s3:DeleteObject",
      "s3:DeleteObjectVersion",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:ListMultipartUploadParts",
      "s3:PutObject",
    ]
    resources = ["${data.aws_s3_bucket.primary.arn}/*"]
  }
}

resource "aws_iam_policy" "bucket_admin" {
  name   = "${local.common_name}-bucket-admin"
  policy = data.aws_iam_policy_document.bucket_admin.json
}

resource "aws_iam_role_policy_attachment" "bucket_admin" {
  policy_arn = aws_iam_policy.bucket_admin.arn
  role       = module.launch_pad.iam_role_id
}
