locals {
  common_name = "${var.application}-${var.environment}-${var.role}"
  common_tags = merge(var.tags, {
    "Role" = var.role
  })

  operators = distinct(var.operators)
  users     = distinct(var.users)
}

module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = var.application
  environment = var.environment
  lifespan    = var.lifespan
  tags        = var.tags
}

module "launch_pad" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/launch-pad.git?ref=v1"

  application = var.application
  environment = var.environment
  role        = var.role

  enable_admin_access = true
  allow_all_egress    = true

  tags = module.tags_base.tags
}

module "cloud_init_dns" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-dns//core?ref=v1"

  zone_name   = data.aws_route53_zone.primary.name
  record_name = var.application
  subnet_type = var.subnet_type
  iam_role_id = module.launch_pad.iam_role_id
}

module "cloud_init_acme_nginx" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-acme-nginx.git?ref=v1"

  region      = var.region
  iam_role_id = module.launch_pad.iam_role_id

  cert_folder  = var.environment
  cert_subject = var.cert_subject
  start_nginx  = false
}

module "standalone_ec2" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2?ref=v2"

  name        = local.common_name
  vpc_id      = data.aws_vpc.primary.id
  subnet_type = var.subnet_type

  spot_price        = var.spot_price
  instance_type     = var.instance_type
  root_block_device = var.root_block_device
  image_id          = data.aws_ami.primary.id
  key_name          = var.key_name

  security_group_ids      = module.launch_pad.security_group_ids
  iam_instance_profile_id = module.launch_pad.iam_instance_profile_id

  rendered_cloud_init = concat([
    module.cloud_init_dns.part,
    module.cloud_init_acme_nginx.part
    ], [{
      filename     = "configure-minecraft.sh"
      content_type = "text/x-shellscript"
      content = templatefile("${path.module}/configure-minecraft.sh", {
        var_region           = var.region
        var_fqdn             = module.cloud_init_dns.fqdn
        var_bucket_id        = data.aws_s3_bucket.primary.id
        var_operators_json   = jsonencode(local.operators)
        var_users_json       = jsonencode(local.users)
        var_enable_whitelist = length(local.users) > 0 ? "true" : "false"
        var_gamemode         = var.gamemode
        var_difficulty       = var.difficulty
        var_motd             = var.motd
        var_max_players      = var.max_players
        var_java_mem         = var.java_mem
        var_rcon_pwd         = var.rcon_pwd
        var_timezone         = var.timezone
      })
  }])

  tags = module.launch_pad.tags
}
