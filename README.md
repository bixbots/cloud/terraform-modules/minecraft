# minecraft

## Summary

Provides terraform modules for deploying a minecraft server. Split into 2 independent modules `bucket` and `server` so that server maintenance, upgrades, etc will preserve files in S3 bucket especially backups.

See README.md in subfolders for details on each module.
