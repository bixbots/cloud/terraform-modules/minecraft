output "bucket_id" {
  description = "The ID of the bucket."
  value       = module.bucket.bucket_id
}

output "bucket_arn" {
  description = "The ARN of the bucket. Will be formated as `arn:aws:s3:::{bucket_id}`."
  value       = module.bucket.bucket_arn
}

output "bucket_domain_name" {
  description = "The bucket domain name. Will be formated as `{bucket_id}.s3.amazonaws.com`."
  value       = module.bucket.bucket_domain_name
}
