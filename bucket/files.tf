locals {
  files = [
    # minecraft
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether-hi_boost_vhi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether-hi_boost_xhi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether-hires.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether-low_boost_hi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether-lowres.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether-vlowres.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/nether.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal-hi_boost_vhi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal-hi_boost_xhi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal-hires.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal-low_boost_hi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal-lowres.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal-vlowres.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/normal.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end-hi_boost_vhi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end-hi_boost_xhi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end-hires.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end-low_boost_hi.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end-lowres.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end-vlowres.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/templates/the_end.txt"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/plugins/dynmap/configuration.txt"
    },
    {
      content_type = "application/x-gzip"
      source       = "${path.module}/files/minecraft/server/plugins/LuckPerms/default.json.gz"
    },
    {
      content_type = "application/octet-stream"
      source       = "${path.module}/files/minecraft/server/plugins/LuckPerms/luckperms-h2.mv.db"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/Dynmap.jar"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/EssentialsX.jar"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/LuckPerms.jar"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/Vault.jar"
    },
    {
      content_type = "text/plain"
      source       = "${path.module}/files/minecraft/server/eula.txt"
    },
    # nginx
    {
      content_type = "text/css"
      source       = "${path.module}/files/nginx/html/404.css"
    },
    {
      content_type = "text/html"
      source       = "${path.module}/files/nginx/html/404.html"
    },
    {
      content_type = "image/ico"
      source       = "${path.module}/files/nginx/html/favicon.ico"
    },
    {
      content_type = "text/html"
      source       = "${path.module}/files/nginx/html/index.css"
    },
    {
      content_type = "text/html"
      source       = "${path.module}/files/nginx/html/index.html"
    },
    {
      content_type = "text/html"
      source       = "${path.module}/files/nginx/html/index.js"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/BiomeFinder.jar"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/ProtocolLib.jar"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/plugins/xRay.jar"
    },
    {
      content_type = "application/java-archive"
      source       = "${path.module}/files/minecraft/server/paperclip.jar"
    },
  ]
}
