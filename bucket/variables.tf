variable "region" {
  type        = string
  default     = "us-east-1"
  description = "(Optional; Default: us-east-1) AWS region where to create resources."
}

variable "lifespan" {
  type        = string
  default     = "permanent"
  description = "(Optional; Default: permanent) The intended lifespan of the resource(s)."
}

variable "application" {
  type        = string
  default     = "minecraft"
  description = "(Optional; Default: minecraft) Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of."
}

variable "role" {
  type        = string
  default     = "server"
  description = "(Optional; Default: server) Name of the role the resource(s) will be performing."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
