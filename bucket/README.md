# minecraft/bucket

## Summary

This module creates an S3 bucket for a minecraft server that contains:

* Initial files (configuration, plugins, nginx, etc) needed for starting the server
* Storage for backups (full and incremental)

## Resources

In addition to these dependent modules:

* [s3-bucket](https://gitlab.com/bixbots/cloud/terraform-modules/s3-bucket)

This module creates the following resources:

* [aws_s3_bucket_object](https://www.terraform.io/docs/providers/aws/r/s3_bucket_object.html)

## Cost

Use of this module will incur fees depending on the storage consumed, access requests, and your lifecycle needs. Before using this module, please familiarize yourself with the expenses associated with S3.

* AWS S3 pricing information is available [here](https://aws.amazon.com/s3/pricing/)

## Inputs

| Name        | Description                                                        | Type          | Default     | Required |
|:------------|:-------------------------------------------------------------------|:--------------|:------------|:---------|
| region      | AWS region where to create resources.                              | `string`      | `us-east-1` | yes      |
| lifespan    | The intended lifespan of the resource(s).                          | `string`      | `permanent` | yes      |
| application | Name of the application the resource(s) will be a part of.         | `string`      | `minecraft` | yes      |
| environment | Name of the environment the resource(s) will be a part of.         | `string`      | -           | yes      |
| role        | Name of the role the resource(s) will be performing.               | `string`      | `server`    | yes      |
| tags        | Additional tags to attach to all resources created by this module. | `map(string)` | `{}`        | no       |

## Outputs

| Name               | Description                                                                 |
|:-------------------|:----------------------------------------------------------------------------|
| bucket_id          | The ID of the bucket.                                                       |
| bucket_arn         | The ARN of the bucket. Will be formated as `arn:aws:s3:::{bucket_id}`.      |
| bucket_domain_name | The bucket domain name. Will be formated as `{bucket_id}.s3.amazonaws.com`. |

## Requirements

| Name      | Version    |
|:----------|:-----------|
| terraform | ~> 0.12.24 |
| aws       | ~> 2.42    |

## Examples

### Simple Usage

```terraform
module "bucket" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/minecraft//bucket?ref=v2"

  region      = "us-east-1"
  lifespan    = "permanent"
  application = "minecraft"
  environment = "example"
  role        = "server"
  tags        = {}
}
```

## Version History

* v1 - Initial Release
* v2 - Use papermc vs spigot and upgrade to 1.16
