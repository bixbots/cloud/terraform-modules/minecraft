locals {
  bucket_name = "${var.environment}-${var.application}-${var.role}"
}

module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = var.application
  environment = var.environment
  lifespan    = var.lifespan
  tags        = var.tags
}

module "bucket" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/s3-bucket?ref=v1"

  region      = var.region
  bucket_name = local.bucket_name

  versioning = {
    enabled    = false
    mfa_delete = false
  }

  admin_policy = {
    enabled     = false
    iam_role_id = ""
  }

  tags = module.tags_base.tags
}

resource "aws_s3_bucket_object" "file" {
  count        = length(local.files)
  bucket       = module.bucket.bucket_id
  key          = trimprefix(local.files[count.index]["source"], "${path.module}/")
  source       = local.files[count.index]["source"]
  content_type = local.files[count.index]["content_type"]
  etag         = filemd5(local.files[count.index]["source"])
}
